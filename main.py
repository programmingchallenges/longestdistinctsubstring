import sys  
from string import ascii_lowercase

def largestSubstring(string, numberOfDistinctCharcters):
    # return firstAttempt(string, numberOfDistinctCharcters)
    return secondAttempt(string, numberOfDistinctCharcters)

def firstAttempt(string, numberOfDistinctCharcters):
    longestSubstringLength = 0
    characterArray = list(string)

    for characterIndex in range(0, len(characterArray)):
        characterMap = (createCharacterMap())
        characterMap[characterArray[characterIndex]] = characterMap[characterArray[characterIndex]] + 1
        substringEndIndex = characterIndex + 1
        while len({k:v for (k,v) in characterMap.items() if v > 0}) <= numberOfDistinctCharcters:
            if substringEndIndex == len(characterArray):
                substringEndIndex += 1
                break
            characterMap[characterArray[substringEndIndex]] = characterMap[characterArray[substringEndIndex]] + 1
            substringEndIndex += 1

        if substringEndIndex - characterIndex - 1 > longestSubstringLength:
            longestSubstringLength = substringEndIndex - characterIndex -1

    return longestSubstringLength

def secondAttempt(string, numberOfDistinctCharcters):
    longestSubstringLength = 0
    characterArray = list(string)

    characterCountsMap = {}
    currentCharacterSubstringLength = 0
    substringStartIndex = 0
    for characterIndex in range(0, len(characterArray)):
        addCharacterToMap(characterCountsMap, characterArray[characterIndex])
        currentCharacterSubstringLength += 1
        if len(characterCountsMap) <= numberOfDistinctCharcters:
            if currentCharacterSubstringLength > longestSubstringLength:
                longestSubstringLength = currentCharacterSubstringLength
        else:
            while len(characterCountsMap) > numberOfDistinctCharcters:
                removeCharacterFromMap(characterCountsMap, characterArray[substringStartIndex])
                substringStartIndex += 1
                currentCharacterSubstringLength -= 1

    return longestSubstringLength

def removeCharacterFromMap(map, character):
    if map[character] == 1:
        map.pop(character)
    else:
        map[character] = map[character] - 1

def addCharacterToMap(map, character):
    if character in map:
        map[character] = map[character] + 1
    else:
        map[character] = 1

def createCharacterMap():
    characterMap = {}
    for character in ascii_lowercase:
        characterMap[character] = 0
    return characterMap

if __name__ == '__main__':
    print('No main for this file')

