import unittest
import main

class TestStringMethods(unittest.TestCase):

    def testEmpty(self):
        self.assertEqual(main.largestSubstring('', 3), 0)

    def testBasic(self):
        self.assertEqual(main.largestSubstring("abcba", 2), 3)

    def testOneCharString(self):
        self.assertEqual(main.largestSubstring("a", 1), 1)

    def testSameCharacterString(self):
        self.assertEqual(main.largestSubstring("aaaaaa", 2), 6)

    def testEfficiency(self):
        testString = ''
        for i in range (0, 100000):
            testString += 'a'
        main.largestSubstring(testString, 2)

if __name__ == '__main__':
    unittest.main()